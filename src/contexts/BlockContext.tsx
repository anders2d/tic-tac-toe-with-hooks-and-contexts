// https://stackoverflow.com/questions/53335907/using-react-context-with-react-hooks-in-typescript
import React, { createContext, Dispatch, SetStateAction, useContext, useEffect, useState } from 'react';
import { coordenates } from '../types/GameTypes';
import { PlayerContext } from './PlayerContext';
interface IState {
  blocks: string[][];
  setBlocks: Dispatch<SetStateAction<IState>>;
  markBlock: (coords: coordenates) => void;
  checkWinner: (coords: coordenates) => boolean
  buildBlocks: () => void

}

const defaultBlockState: IState = {
  blocks: [],
  setBlocks: (): void => { },
  markBlock: () => { },
  checkWinner: (coords: coordenates) => false,
  buildBlocks: () => { }
}

export const BlockContext = createContext<IState>(defaultBlockState);

const trikeSize = 3

const listOfMovements = {
  moveToRight: (coords: coordenates) => ({ ...coords, 'y': coords.y + 1 }),
  moveToDown: (coords: coordenates) => ({ ...coords, 'x': coords.x + 1 }),
  moveDiagLeftToRight: (coords: coordenates) => ({ 'x': coords.x + 1, 'y': coords.y + 1 }),
  moveDiagRightToLeft: (coords: coordenates) => ({ 'x': coords.x + 1, 'y': coords.y - 1 }),
}
const BlockContextProvider: React.FC = (props: any) => {
  const [blocks, setBlocks] = useState<string[][]>([[]])
  const { player } = useContext(PlayerContext)

  useEffect(() => {
    buildBlocks()
    console.log("test")
  }, [])


  const markBlock = (coords: coordenates) => {
    let newBlocks = [...blocks]
    newBlocks[coords.x][coords.y] = player.name
    setBlocks(newBlocks)
    checkWinner(coords)
  }

  const checkWinner = (coords: coordenates): boolean => {
    let movements = [
      { move: listOfMovements.moveToRight, start: { ...coords, 'y': 0 } },
      { move: listOfMovements.moveToDown, start: { ...coords, 'x': 0 } }
    ]
    if (coords.x === coords.y) {
      movements.push({ move: listOfMovements.moveDiagLeftToRight, start: { 'x': 0, 'y': 0 } })
    }

    if (trikeSize - 1 - coords.x === coords.y) {
      movements.push({ move: listOfMovements.moveDiagRightToLeft, start: { 'x': 0, 'y': trikeSize - 1 } })
    }


    const getMatchs = (move: Function, startCoords: coordenates) => {
      let count = 0;
      let curCoords: coordenates = startCoords;
      for (let i = 0; i < trikeSize; i++) {
        if (getLetterInCoords(curCoords) === player.name) {
          count++;
        }
        curCoords = move(curCoords)
      }
      return count;
    }

    const isWinner = !movements.every((movement) => {
      let matchs = getMatchs(movement.move, movement.start)
      return matchs !== trikeSize
    })

    console.log({ isWinner })
    return isWinner
  }



  const getLetterInCoords = (coords: coordenates) => {
    return blocks[coords.x][coords.y]
  }

  const buildBlocks = () => {

    setBlocks(Array.from({ length: 3 }, () => Array.from({ length: 3 }, () => "")))
  }

  return (
    <BlockContext.Provider value={{
      ...defaultBlockState,
      blocks,
      markBlock,
      checkWinner,
      buildBlocks
    }}>
      {props.children}

    </BlockContext.Provider>
  )
}

export default BlockContextProvider;
