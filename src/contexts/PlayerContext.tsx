// https://stackoverflow.com/questions/53335907/using-react-context-with-react-hooks-in-typescript
import React, { createContext, Dispatch, SetStateAction, useState } from 'react';


const Player = {
    one: "x",
    two: "O"
}

interface IPlayer {
    name: string
    isWinner: boolean
}



interface IState {
    player: IPlayer,
    setPlayer: Dispatch<SetStateAction<IState>>,
    togglePlayer: () => void,
    setCurrentPlayerAsWinner: () => void,
    resetPlayerState: () => void
}


const defaultPlayerState: IState = {
    player: {name:"x"} as IPlayer,
    setPlayer: () => { },
    togglePlayer: () => { },
    setCurrentPlayerAsWinner: () => { },
    resetPlayerState: () => {}
}
export const PlayerContext = createContext<IState>(defaultPlayerState);


const PlayerContextProvider: React.FC = (props: any) => {
    const playerState: IPlayer = {
        name: Player.one,
        isWinner: false
    }
    const [player, setPlayer] = useState<IPlayer>(playerState)
    const togglePlayer = () => {
        const newPlayerName = player.name === Player.one ? Player.two : Player.one
        setPlayer({ ...player, name: newPlayerName })
    }

    const resetPlayerState = () => {
        setPlayer(defaultPlayerState.player)
    }
    const setCurrentPlayerAsWinner = () => {
        
        setPlayer({ ...player, isWinner: true })
        console.log(player)
    }
    
    return (
        <PlayerContext.Provider value={{
            ...defaultPlayerState,
            player,
            togglePlayer,
            setCurrentPlayerAsWinner,
            resetPlayerState
        }}>
            {props.children}
        </PlayerContext.Provider>
    )
}

export default PlayerContextProvider;
