import React, { useContext } from 'react'
import { BlockContext } from '../contexts/BlockContext'
import { map } from 'lodash'
import { PlayerContext } from '../contexts/PlayerContext'

import { coordenates } from '../types/GameTypes'

const Block = (): JSX.Element => {
    const { blocks, markBlock, checkWinner, buildBlocks } = useContext(BlockContext)
    const { 
        player,
        togglePlayer,
        setCurrentPlayerAsWinner,
        resetPlayerState} = useContext(PlayerContext)

    const handleClick = (coords: coordenates) => {
        if (player.isWinner) {
            return
        }
        markBlock(coords)
        if (checkWinner(coords)) {
            console.log("best test 1")
            setCurrentPlayerAsWinner()
        } else {
            togglePlayer()
            console.log("best test2")
        }

    }

    const resetGame = () => {
        buildBlocks()
        resetPlayerState()
    }
    return (
        <>
            {blocks.map((row, xKey) => {
                const rows = map(row, (col, yKey) => {
                    return (<div onClick={() => { handleClick({ x: xKey, y: yKey }) }} className="block"> {col}</div>)
                })
                return (<div className="rowBlock"> {rows}</div>)
            })}
            <button onClick={resetGame}> Reset game </button>
        </>
    )
}
export default Block;
