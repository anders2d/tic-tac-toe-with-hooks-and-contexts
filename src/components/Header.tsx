import React, {useContext} from 'react'

import { PlayerContext } from '../contexts/PlayerContext'
const Header = ():JSX.Element => {
    const {player} = useContext(PlayerContext)

    return(
        <>
        <h1>{player.isWinner? "Winner:": "Player: "} {player.name}</h1>
        {console.log({player})}
        </>
        )
}
export default Header;
