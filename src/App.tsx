import React from 'react';
import './App.css';
import Blocks from './components/Blocks';
import BlockContextProvider from './contexts/BlockContext';
import PlayerContextProvider from './contexts/PlayerContext';
import Header from './components/Header';



function App() {


  return (
    <div className="App">
      <header className="App-header">
        <PlayerContextProvider>
          <Header/>
          
          <BlockContextProvider>
            <Blocks />
          </BlockContextProvider>
        
        </PlayerContextProvider>
      </header>
    </div>
  );
}

export default App;
